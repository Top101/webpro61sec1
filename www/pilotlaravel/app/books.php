<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class books extends Model
{
    //
    protected $table = "books";
    protected  $primaryKey = "isbn_no";
    public $incrementing = false;
    protected $fillable = ['name','cetegory','description'];
}
